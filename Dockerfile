FROM nginx

# Copy the custom index.html file from the current directory into the container
COPY index.html /usr/share/nginx/html/

# Expose port 80 to allow external access to the Nginx server
EXPOSE 80
